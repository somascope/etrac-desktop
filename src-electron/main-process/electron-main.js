import { app, BrowserWindow } from 'electron'

/**
 * Set `__statics` path to static files in production;
 * The reason we are setting it here is that the path needs to be evaluated at runtime
 */
if (process.env.PROD) {
  global.__statics = require('path').join(__dirname, 'statics').replace(/\\/g, '\\\\')
}

let mainWindow
const shell = require('electron').shell
const appWidth = 1250
const appHeight = 680
const appWidthMax = 2560
const appHeightMax = 1440

function createWindow () {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    width: appWidth,
    height: appHeight,
    minWidth: appWidth,
    minHeight: appHeight,
    maxWidth: appWidthMax,
    maxHeight: appHeightMax,
    useContentSize: true
  })
  mainWindow.setMenu(null)

  mainWindow.loadURL(process.env.APP_URL)

  mainWindow.webContents.on('new-window', function (e, url) {
    e.preventDefault()
    shell.openExternal(url)
  })

  mainWindow.on('closed', () => {
    mainWindow = null
  })
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})
