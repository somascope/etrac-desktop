const fs = require('fs')

const courseFolders = ['course1', 'course2', 'course3', 'course4', 'course5', 'course6']
const courseData = fs.readFileSync('src/assets/courseData.json', 'utf8')

function updateFile (fileObj, courseNum) {
  const resumeStr = '"resume":{"id":'
  let idx = fileObj.indexOf(resumeStr)
  let resumeId = getCourseId(courseNum)
  let str1 = fileObj.substring(0, idx)
  let str2 = `"resume":{"id":"${resumeId}"`
  console.log(` - Set the resume ID for course${courseNum} to ${resumeId}`)
  let str3 = fileObj.substr(idx + str2.length)
  return str1 + str2 + str3
}

function getCourseId (courseNum) {
  const courses = JSON.parse(courseData).courses
  const course = courses.find(course => course.id === courseNum)
  return course.storageKey
}

function processCourses () {
  console.log('Set any new Storyline resume IDs to their originals (from courseData.json).')
  courseFolders.forEach(course => {
    const filePath = 'src/statics/courses/' + course + '/html5/data/js/data.js'
    // const filePathTest = 'src/statics/courses/' + course + '/html5/data/js/dataTest.js'
    const fileData = fs.readFileSync(filePath, 'utf8')
    const courseNum = parseInt(course.substring(course.length - 1))

    try {
      fs.writeFileSync(filePath, updateFile(fileData, courseNum))
    } catch (error) {
      console.log(error)
    }
  })
  console.log(`The ${courseFolders.length} Storyline courses have been updated to have their previous IDs.`)
}
processCourses()
