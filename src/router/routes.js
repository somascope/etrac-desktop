// import store from '../store'
export default [
  {
    path: '/',
    component: () => import('layouts/default'),
    children: [
      {
        path: '',
        component: () => import('pages/Menu')
      }
    ]
  },
  {
    path: '/course0',
    component: () => import('layouts/Course'),
    children: [
      {
        path: '',
        component: () => import('pages/course0')
      }
    ]
  },
  {
    path: '/course1',
    component: () => import('layouts/Course'),
    children: [
      {
        path: '',
        component: () => import('pages/course1')
      }
    ]
  },
  {
    path: '/course2',
    component: () => import('layouts/Course'),
    children: [
      {
        path: '',
        component: () => import('pages/course2')
      }
    ]
  },
  {
    path: '/course3',
    component: () => import('layouts/Course'),
    children: [
      {
        path: '',
        component: () => import('pages/course3')
      }
    ]
  },
  {
    path: '/course4',
    component: () => import('layouts/Course'),
    children: [
      {
        path: '',
        component: () => import('pages/course4')
      }
    ]
  },
  {
    path: '/course5',
    component: () => import('layouts/Course'),
    children: [
      {
        path: '',
        component: () => import('pages/course5')
      }
    ]
  },
  {
    path: '/course6',
    component: () => import('layouts/Course'),
    children: [
      {
        path: '',
        component: () => import('pages/course6')
      }
    ]
  },
  { // Always leave this as last one
    path: '*',
    component: () => import('pages/404')
  }
]
