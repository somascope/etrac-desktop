import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'
import data from '../assets/courseData.json'

Vue.use(Vuex)

const vuexPersist = new VuexPersist({
  key: 'etrac-desktop',
  storage: window.localStorage
})

const store = new Vuex.Store({
  strict: true,
  state: {
    testMode: [],
    todayTestDate: null,
    email: '',
    isActivated: false,
    isExpired: false,
    showConfirmation: false,
    codeIsEntered: false,
    activationConfirmed: false,
    firstLaunch: false,
    courses: data.courses,
    users: data.users,
    renewalDate: null,
    subscription: {
      lifetime: false,
      currentYear: 1,
      year1: {
        code: null,
        activatedDate: null,
        expiredDate: null
      },
      year2: {
        code: null,
        activatedDate: null,
        expiredDate: null
      },
      year3: {
        code: null,
        activatedDate: null,
        expiredDate: null
      },
      year4: {
        code: null,
        activatedDate: null,
        expiredDate: null
      }
    }
  },
  mutations: {
    courses (state, payload) {
      state.courses = payload
    },
    testMode (state, payload) {
      state.testMode = payload
    },
    todayTestDate (state, payload) {
      state.todayTestDate = payload
    },
    updateUser (state, payload) {
      const user = state.users.find(user => user.id === payload.id)
      user.name = payload.name
      user.image = payload.image
      user.progress = payload.progress
    },
    setEmail (state, payload) {
      state.email = payload
    },
    isActivated (state, payload) {
      state.isActivated = payload
    },
    isExpired (state, payload) {
      state.isExpired = payload
    },
    isLifetime (state, payload) {
      state.subscription.lifetime = payload
    },
    showConfirmation (state, payload) {
      state.showConfirmation = payload
    },
    activationConfirmed (state, payload) {
      state.activationConfirmed = payload
    },
    codeIsEntered (state, payload) {
      state.codeIsEntered = payload
    },
    firstLaunch (state, payload) {
      state.firstLaunch = payload
    },
    setCurrentYear (state, payload) {
      state.subscription.currentYear = payload
    },
    renewalDate (state, payload) {
      state.renewalDate = payload
    },
    recordActivationYear1 (state, payload) {
      const subscription = state.subscription.year1
      subscription.code = payload.code
      subscription.activatedDate = payload.activatedDate
      subscription.expiredDate = payload.expiredDate
    },
    recordActivationYear2 (state, payload) {
      const subscription = state.subscription.year2
      subscription.code = payload.code
      subscription.activatedDate = payload.activatedDate
      subscription.expiredDate = payload.expiredDate
    },
    recordActivationYear3 (state, payload) {
      const subscription = state.subscription.year3
      subscription.code = payload.code
      subscription.activatedDate = payload.activatedDate
      subscription.expiredDate = payload.expiredDate
    },
    recordActivationYear4 (state, payload) {
      const subscription = state.subscription.year4
      subscription.code = payload.code
      subscription.activatedDate = payload.activatedDate
      subscription.expiredDate = payload.expiredDate
    }
  },
  getters: {
    getCurrentUser: (state) => (id) => {
      return state.users.find(user => user.id === id)
    },
    getCurrentCourse: (state) => (id) => {
      return state.courses.find(course => course.id === id)
    },
    getCurrentSubscription: (state) => {
      if (state.subscription.currentYear === 1) {
        return state.subscription.year1
      } else if (state.subscription.currentYear === 2) {
        return state.subscription.year2
      } else if (state.subscription.currentYear === 3) {
        return state.subscription.year3
      } else if (state.subscription.currentYear === 4) {
        return state.subscription.year4
      }
    }
  },
  plugins: [vuexPersist.plugin]
})

export default store
