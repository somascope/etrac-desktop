const chance = require('chance').Chance()

export default function createFakeUser () {
  let noUser = chance.natural({ min: 0, max: 4 })
  let gender = ''
  let number = 0
  let name = ''
  let image = ''
  let progress = 0

  if (noUser === 0) {
    name = 'No User'
    image = 'statics/avatars/avatarHead-noUser.png'
    progress = 0
  } else {
    gender = chance.natural({ min: 0, max: 1 }) === 1 ? 'male' : 'female'
    number = chance.natural({ min: 1, max: 6 })
    name = chance.name({ gender: gender, prefix: false, middle: false })
    image = 'statics/avatars/avatarHead-' + gender.substr(0, 1) + number + '.png'
    progress = chance.natural({ min: 0, max: 99 })
  }

  return {
    name: name,
    image: image,
    progress: progress
  }
}
