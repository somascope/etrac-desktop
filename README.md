# eTrac Desktop 2.0

Version 2.0 is the re-built version of the Adobe AIR & Flash-based version of the program.

This is built using the [Quasar framework](https://quasar-framework.org/).

## Project setup

- Clone (or update from) the repo
- Add the content files that are outside of this repo (see below)
- npm install

## Be sure to have the Quasar CLI installed

- https://quasar.dev/quasar-cli/installation
- npm install -g @quasar/cli

## Course content files that are outside of this repo

See .gitignore for details.

- /src/statics/**courses** - These are too large, too numerous, and constantly updated outside of any version control workflow. They are managed locally and in Dropbox: "Zenmation\etrac desktop 2\Published Desktop Files".
- /src/statics/**video** - This exists in Dropbox: Zenmation\etrac desktop 2\assets\clouds

## When there are course updates

- Replace the appropriate course files in /src/statics/courses/
- Update the courses' storage ID (for resume purposes) via: npm run script

## For each new content update (to the courses)

- Increment the version number (in package.json)
- Ex: For a content update (such as updated Storyline files), change "2.0.1" to "2.0.2", etc.

## Run the update script to modify course resume IDs

- \$ npm run update
- This changes the newly-updated course files with their new resume IDs to the old course IDs, which is what existing user's procgress will depend on.
- Always test this by first recording course progress (%) before making edits, and then checking these are the same after updates are completed.

## Dev and build commands

- For development preview: quasar dev -m electron
- For full build: quasar build -m electron

## Distribution

- The build is for Windows only, not MacOS
- Locate the EXE file at: dist\electron-mat\Packaged\eTrac Desktop 2.0 Setup 2.0.2.exe
- Provide this EXE file to the client, usually via Dropbox
- To run without installing: dist\electron-mat\Packaged\win-unpacked\eTrac Desktop 2.0.exe
